
public class Main {
	
	public static void main(String[] args){
		User x = new User("andrea20", "andrea123");
		if(x.isConnected()) {
			System.out.println("User is connected!");
		} else {
			System.out.println("Is not connected!");
		}
		
		// static way
		if(User.isConnected("luca", "luca123")) {
			System.out.println("is connected");		
			Category c = new Category();
			c.CreateCategory();
		} else {
			System.out.println("Is not connected!");
		}
	}
}
